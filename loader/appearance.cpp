#include "loader.h"

void loader::createStylesheets()
{
    whitePushButtonsStylesheet="QPushButton{   \
                                background-color: #2E2E2E; \
                                border-style: outset;  \
                                border-width: 2px;     \
                                border-radius: 5px;   \
                                border-color: #AAAAAA;   \
                                font-size: 13px;font-family: Rubik;font-weight: bold;\
                                color: #AAAAAA;\
                                min-width: 150px;min-height: 35px;\
                            }\
                            QPushButton:hover{   \
                                background-color: rgba(170,170,170,0.15); \
                                border-color: #AAAAAA;   \
                                color: #FFFFFF;\
                            }";
    bluePushButtonsStylesheet="QPushButton{   \
                                background-color: #42abdb; \
                                border-style: solid;  \
                                border-width: 2px;     \
                                border-radius: 5px;   \
                                border-color: #42abdb;   \
                                font-size: 12px;font-family: Rubik;font-weight: bold;\
                                color: white;\
                                min-width: 150px;min-height: 35px;\
                            }\
                            QPushButton:disabled{   \
                                background-color: #2d7495; \
                                border-color: #2d7495;   \
                                color: #adadad;\
                            }\
                            QPushButton:hover{   \
                                    background-color: #2890CC; \
                                    border-color: #2890CC;   \
                                    color: white;\
                            }";

    rubykRegularBlueLabelStylesheet="QLabel{font-size: 18px;font-family: Rubik; color: #42abdb;}";
    rubykRegularWhiteLabelStylesheet="QLabel{font-size: 18px;font-family: Rubik; color: white;}";
    rubykItalicWhiteLabelStylesheet="QLabel{font-size: 18px;font-family: Rubik;font-style: italic;color: white;}";

    dialogStyleSheet = "QMessageBox {\
                            background-color: #2E2E2E;\
                            min-width: 600px;\
                        }\
                        QMessageBox QLabel {\
                            min-width:300 px;\
                            font-size: 18px;font-family: Rubik; color: white;\
                        }";
    dialogStyleSheet += "QMessageBox " + whitePushButtonsStylesheet;

    progressStyleSheet = "QProgressDialog {\
                            background-color: #2E2E2E;\
                        }\
                        QProgressDialog QProgressBar::chunk {\
                            background-color: #42abdb;\
                            text-align: right;\
                        }\
                        QProgressDialog QLabel {\
                            font-size: 18px;font-family: Rubik; color: white;\
                        }";

    this->setStyleSheet("QMainWindow{background-color: #2E2E2E;}\
                            QMenuBar {\
                                font-size: 12px;font-family: Rubik;\
                                background-color: qlineargradient(x1:0, y1:0, x2:0, y2:1,stop:0 lightgray, stop:1 darkgray);\
                                spacing: 3px; /* spacing between menu bar items */\
                            }\
                            QMenu {\
                                background-color: #AAAAAA; \
                                border: 1px solid black;\
                                font-size: 12px;font-family: Rubik;\
                            }\
                            QInputDialog {background-color: #2E2E2E;color: white;}\
                            QInputDialog QLabel{font-size: 18px;font-family: Rubik; color: white;}\
                            QInputDialog "+whitePushButtonsStylesheet+\
                            dialogStyleSheet);

    ui->progressBar->setStyleSheet("QProgressBar {\
                                       background-color: white;\
                                       border-radius: 5px;   \
                                       border-color: #AAAAAA; \
                                    }\
                                    QProgressBar::chunk {\
                                        background-color: #42abdb;\
                                        border-radius: 5px;   \
                                        border-color: #AAAAAA; \
                                    }\
                                    QLabel {\
                                        font-size: 18px;font-family: Rubik; color: white;\
                                    }");
    QListView *comboitems = new QListView(ui->midiportsCombo);
    comboitems->setStyleSheet("QListView::item{height: 50px;}");
    ui->midiportsCombo->setView(comboitems);
    ui->midiportsCombo->setStyleSheet("QComboBox {\
                                          font-size: 18px;font-family: Rubik;\
                                          border: 1px solid gray;\
                                          border-radius: 0px;\
                                          padding: 1px 18px 1px 3px;\
                                          min-width: 6em;\
                                          height : 35px;\
                                      }\
                                     QComboBox QAbstractItemView{ \
                                          font-size: 18px;font-family: Rubik;\
                                          border: 2px solid darkgray;  \
                                          selection-background-color: #42abdb;\
                                     }\
                                     QComboBox::drop-down{\
                                         border: 0px;\
                                         width: 40px;\
                                         height : 35px;\
                                     }\
                                     QComboBox::down-arrow {\
                                          image: url(:/img/arrow-down.png);\
                                     }");

    ui->loadFilePushButton->setStyleSheet(whitePushButtonsStylesheet);
    ui->sendPushButton->setStyleSheet(bluePushButtonsStylesheet);

    ui->portsLabel->setStyleSheet(rubykRegularWhiteLabelStylesheet);
    ui->firmwareSelectLabel->setStyleSheet(rubykRegularWhiteLabelStylesheet);
    ui->statusLabel->setStyleSheet(rubykItalicWhiteLabelStylesheet);

    ui->gridLayout->setColumnStretch(0,1);
    ui->gridLayout->setColumnStretch(1,0);
    ui->gridLayout->setColumnStretch(2,2);

    ui->progressBar->setAlignment(Qt::AlignCenter);
    ui->progressBar->setFixedSize(ui->sendPushButton->width(),ui->sendPushButton->height());
    ui->progressBar->hide();
}
