/********************************************************************************
** Form generated from reading UI file 'loader.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOADER_H
#define UI_LOADER_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FirmwareLoader
{
public:
    QAction *actionOpen;
    QAction *actionSaveAs;
    QAction *actionExit;
    QAction *actionSave;
    QAction *actionFirmware_Update;
    QAction *actionNew;
    QAction *actionAux_Firmware_Update;
    QAction *actionEEPROM_erase;
    QAction *actionAbout;
    QAction *actionBuild_update_file;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *mainVerticalLayout;
    QHBoxLayout *h1Layout;
    QVBoxLayout *verticalLayout_2;
    QGridLayout *gridLayout;
    QLabel *firmwareSelectLabel;
    QComboBox *midiportsCombo;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *loadFilePushButton;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *sendPushButton;
    QProgressBar *progressBar;
    QSpacerItem *horizontalSpacer_2;
    QLabel *portsLabel;
    QLabel *infoLabel;
    QLabel *statusLabel;
    QMenuBar *menuBar;
    QMenu *menuoctaManager;
    QMenu *optionsMenu;

    void setupUi(QMainWindow *FirmwareLoader)
    {
        if (FirmwareLoader->objectName().isEmpty())
            FirmwareLoader->setObjectName(QString::fromUtf8("FirmwareLoader"));
        FirmwareLoader->resize(440, 247);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(FirmwareLoader->sizePolicy().hasHeightForWidth());
        FirmwareLoader->setSizePolicy(sizePolicy);
        QIcon icon;
        icon.addFile(QString::fromUtf8("logo.png"), QSize(), QIcon::Normal, QIcon::Off);
        FirmwareLoader->setWindowIcon(icon);
        actionOpen = new QAction(FirmwareLoader);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        actionSaveAs = new QAction(FirmwareLoader);
        actionSaveAs->setObjectName(QString::fromUtf8("actionSaveAs"));
        actionExit = new QAction(FirmwareLoader);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionSave = new QAction(FirmwareLoader);
        actionSave->setObjectName(QString::fromUtf8("actionSave"));
        actionFirmware_Update = new QAction(FirmwareLoader);
        actionFirmware_Update->setObjectName(QString::fromUtf8("actionFirmware_Update"));
        actionNew = new QAction(FirmwareLoader);
        actionNew->setObjectName(QString::fromUtf8("actionNew"));
        actionAux_Firmware_Update = new QAction(FirmwareLoader);
        actionAux_Firmware_Update->setObjectName(QString::fromUtf8("actionAux_Firmware_Update"));
        actionEEPROM_erase = new QAction(FirmwareLoader);
        actionEEPROM_erase->setObjectName(QString::fromUtf8("actionEEPROM_erase"));
        actionEEPROM_erase->setVisible(true);
        actionAbout = new QAction(FirmwareLoader);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionBuild_update_file = new QAction(FirmwareLoader);
        actionBuild_update_file->setObjectName(QString::fromUtf8("actionBuild_update_file"));
        centralWidget = new QWidget(FirmwareLoader);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy1);
        centralWidget->setMaximumSize(QSize(1000, 1000));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        mainVerticalLayout = new QVBoxLayout();
        mainVerticalLayout->setSpacing(6);
        mainVerticalLayout->setObjectName(QString::fromUtf8("mainVerticalLayout"));
        h1Layout = new QHBoxLayout();
        h1Layout->setSpacing(6);
        h1Layout->setObjectName(QString::fromUtf8("h1Layout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        firmwareSelectLabel = new QLabel(centralWidget);
        firmwareSelectLabel->setObjectName(QString::fromUtf8("firmwareSelectLabel"));
        firmwareSelectLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(firmwareSelectLabel, 1, 0, 1, 1);

        midiportsCombo = new QComboBox(centralWidget);
        midiportsCombo->setObjectName(QString::fromUtf8("midiportsCombo"));
        midiportsCombo->setMinimumSize(QSize(300, 0));

        gridLayout->addWidget(midiportsCombo, 0, 1, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        loadFilePushButton = new QPushButton(centralWidget);
        loadFilePushButton->setObjectName(QString::fromUtf8("loadFilePushButton"));
        sizePolicy.setHeightForWidth(loadFilePushButton->sizePolicy().hasHeightForWidth());
        loadFilePushButton->setSizePolicy(sizePolicy);
        loadFilePushButton->setMinimumSize(QSize(150, 35));
        loadFilePushButton->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_2->addWidget(loadFilePushButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);


        gridLayout->addLayout(horizontalLayout_2, 1, 1, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        sendPushButton = new QPushButton(centralWidget);
        sendPushButton->setObjectName(QString::fromUtf8("sendPushButton"));
        sizePolicy.setHeightForWidth(sendPushButton->sizePolicy().hasHeightForWidth());
        sendPushButton->setSizePolicy(sizePolicy);
        sendPushButton->setMinimumSize(QSize(150, 35));
        sendPushButton->setMaximumSize(QSize(100, 16777215));

        horizontalLayout_3->addWidget(sendPushButton);

        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setValue(24);

        horizontalLayout_3->addWidget(progressBar);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        gridLayout->addLayout(horizontalLayout_3, 5, 1, 1, 1);

        portsLabel = new QLabel(centralWidget);
        portsLabel->setObjectName(QString::fromUtf8("portsLabel"));
        portsLabel->setMaximumSize(QSize(16777215, 16777215));
        portsLabel->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(portsLabel, 0, 0, 1, 1);

        infoLabel = new QLabel(centralWidget);
        infoLabel->setObjectName(QString::fromUtf8("infoLabel"));

        gridLayout->addWidget(infoLabel, 2, 1, 1, 1);

        statusLabel = new QLabel(centralWidget);
        statusLabel->setObjectName(QString::fromUtf8("statusLabel"));
        statusLabel->setEnabled(true);
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(statusLabel->sizePolicy().hasHeightForWidth());
        statusLabel->setSizePolicy(sizePolicy2);
        statusLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        gridLayout->addWidget(statusLabel, 3, 1, 1, 1);


        verticalLayout_2->addLayout(gridLayout);


        h1Layout->addLayout(verticalLayout_2);


        mainVerticalLayout->addLayout(h1Layout);

        mainVerticalLayout->setStretch(0, 3);

        verticalLayout->addLayout(mainVerticalLayout);

        FirmwareLoader->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(FirmwareLoader);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 440, 21));
        menuoctaManager = new QMenu(menuBar);
        menuoctaManager->setObjectName(QString::fromUtf8("menuoctaManager"));
        optionsMenu = new QMenu(menuBar);
        optionsMenu->setObjectName(QString::fromUtf8("optionsMenu"));
        FirmwareLoader->setMenuBar(menuBar);

        menuBar->addAction(menuoctaManager->menuAction());
        menuBar->addAction(optionsMenu->menuAction());
        menuoctaManager->addSeparator();
        menuoctaManager->addSeparator();
        menuoctaManager->addSeparator();
        menuoctaManager->addAction(actionAbout);
        menuoctaManager->addSeparator();
        menuoctaManager->addAction(actionExit);
        optionsMenu->addAction(actionEEPROM_erase);
        optionsMenu->addAction(actionBuild_update_file);

        retranslateUi(FirmwareLoader);

        QMetaObject::connectSlotsByName(FirmwareLoader);
    } // setupUi

    void retranslateUi(QMainWindow *FirmwareLoader)
    {
        FirmwareLoader->setWindowTitle(QCoreApplication::translate("FirmwareLoader", "YTXV2 firmware loader", nullptr));
        actionOpen->setText(QCoreApplication::translate("FirmwareLoader", "&Open From...", nullptr));
        actionSaveAs->setText(QCoreApplication::translate("FirmwareLoader", "Save &As...", nullptr));
        actionSaveAs->setIconText(QCoreApplication::translate("FirmwareLoader", "Save As...", nullptr));
        actionExit->setText(QCoreApplication::translate("FirmwareLoader", "Exit", nullptr));
        actionSave->setText(QCoreApplication::translate("FirmwareLoader", "&Save", nullptr));
        actionSave->setIconText(QCoreApplication::translate("FirmwareLoader", "Save", nullptr));
        actionFirmware_Update->setText(QCoreApplication::translate("FirmwareLoader", "Main Firmware Update", nullptr));
        actionNew->setText(QCoreApplication::translate("FirmwareLoader", "New", nullptr));
        actionAux_Firmware_Update->setText(QCoreApplication::translate("FirmwareLoader", "Aux Firmware Update", nullptr));
        actionEEPROM_erase->setText(QCoreApplication::translate("FirmwareLoader", "EEPROM erase", nullptr));
        actionAbout->setText(QCoreApplication::translate("FirmwareLoader", "About", nullptr));
        actionBuild_update_file->setText(QCoreApplication::translate("FirmwareLoader", "Build firmware file", nullptr));
        firmwareSelectLabel->setText(QCoreApplication::translate("FirmwareLoader", "Firmware", nullptr));
        loadFilePushButton->setText(QCoreApplication::translate("FirmwareLoader", "Load File...", nullptr));
        sendPushButton->setText(QCoreApplication::translate("FirmwareLoader", "SEND TO DEVICE", nullptr));
        portsLabel->setText(QCoreApplication::translate("FirmwareLoader", "MIDI device", nullptr));
        infoLabel->setText(QCoreApplication::translate("FirmwareLoader", "Info", nullptr));
        statusLabel->setText(QCoreApplication::translate("FirmwareLoader", "Status", nullptr));
        menuoctaManager->setTitle(QCoreApplication::translate("FirmwareLoader", "Loader", nullptr));
        optionsMenu->setTitle(QCoreApplication::translate("FirmwareLoader", "&Tools", nullptr));
    } // retranslateUi

};

namespace Ui {
    class FirmwareLoader: public Ui_FirmwareLoader {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOADER_H
