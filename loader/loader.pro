#-------------------------------------------------
#
# Project created by QtCreator 2016-09-23T16:48:24
#
#-------------------------------------------------

#CONFIG += jack
CONFIG+=release
QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app

RESOURCES += loader.qrc

SOURCES += actions.cpp\
    appearance.cpp \
        main.cpp\
        loader.cpp\
        midi.cpp\
        file.cpp\
        RtMidi.cpp \
    firmwareupdate.cpp

HEADERS  += loader.h\
            defines.h\
            RtMidi.h

FORMS    += loader.ui

unix {
    macx{
        TARGET = YTXFirmwareManager
        ICON = img/icono.icns
        DEFINES += __MACOSX_CORE__
        LIBS += -framework CoreMIDI -framework CoreAudio -framework CoreFoundation
    }
    else{
        DESTDIR = release
        jack {
            TARGET = ytxFirmwareManager-jack
            DEFINES += __UNIX_JACK__
            LIBS += -ljack
        }
        else {
            TARGET = ytxFirmwareManager-alsa
            DEFINES += __LINUX_ALSA__
            LIBS += -lasound -lpthread
        }
    }
}

win32{
    TARGET = ytxFirmwareManager-win32
    DEFINES += __WINDOWS_MM__
    LIBS += -lwinmm
    RC_ICONS = img/icon.ico
}



