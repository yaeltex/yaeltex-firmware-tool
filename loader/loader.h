#ifndef LOADER_H
#define LOADER_H

#include "ui_loader.h"
#include "defines.h"

#include <QInputDialog>
#include <QShortcut>
#include <QMainWindow>
#include <QTranslator>
#include <QApplication>
#include <QFontDatabase>
#include <iostream>
#include <cstdlib>
#include <signal.h>
#include "RtMidi.h"
#include <QDebug>
#include <QTimer>
#include <QAction>
#include <QContextMenuEvent>
#include <QShortcut>
#include <QFile>
#include <QFileDialog>
#include <QDir>
#include <QSortFilterProxyModel>
#include <QFileSystemModel>
#include <QPushButton>
#include <QSlider>
#include <QLayout>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QRadioButton>
#include <QLabel>
#include <QButtonGroup>
#include <QGroupBox>
#include <QStyledItemDelegate>
#include <QPainter>
#include <QTime>
#include <QListWidget>
#include <QProxyStyle>
#include <QSignalMapper>
#include <QGridLayout>
#include <QSpinBox>
#include <QSizePolicy>
#include <QDir>
#include <stdint.h>
#include <QMessageBox>
#include <QDialogButtonBox>
#include <QProgressDialog>
#include <QProgressBar>
#include <QThread>
#include <QSplashScreen>
#include <QLineEdit>
#include <QScrollBar>
#include <QRadioButton>
#include <QHeaderView>
#include <QUndoCommand>
#include <QPoint>
#include <QComboBox>
#include <QStandardPaths>


//LOADER STATEs
enum ytxState
{
    INIT,
    DISCONNECTED,
    CONNECTING,
    CONNECTED,
    READY_TO_SEND,
    UPDATING,
    UPLOADED
};

typedef struct __attribute__((packed, aligned(1)))
{
    char id[4];
    char version[10];
}ytxHeaderType;

typedef struct __attribute__((packed, aligned(1)))
{
    char signature[4];
    uint32_t sizeMain;
    uint32_t sizeAux;
}ytxUpdateHeaderType;

typedef struct
{
    char path[300];
    char language[5];
    int x;
    int y;
    int width;
    int height;
}contextType;

QT_USE_NAMESPACE
namespace Ui {
class FirmwareLoader;
}

class loader : public QMainWindow
{
    Q_OBJECT

public:
    explicit loader(QWidget *parent = 0);
    ~loader();

private slots:
    void midiPull();

    void EEPROM_erase();

    void buildUpdate();

    void firmwareUpdate();

    void loadFirmwareFile();

    void connectToDevice(int index);

    void resetDeviceIntoBootloader();

    void midiSendTimeout();

    void searchPorts();

    void checkMidiConection();

    void on_midiportsCombo_currentIndexChanged(int index);

private:

    Ui::FirmwareLoader *ui;

    //Styles
    QString spinBoxstyleSheet,labelStyleSheet,containerStyleSheet;
    QString bluePushButtonsStylesheet,whitePushButtonsStylesheet;
    QString rubykRegularWhiteLabelStylesheet,rubykItalicWhiteLabelStylesheet,rubykRegularBlueLabelStylesheet;
    QString dialogStyleSheet,progressStyleSheet;

    //Midi Objects
    RtMidiIn  *midiin;
    RtMidiOut *midiout;

    QStringList *inputPortsName;
    QStringList *outputPortsName;

    //Actions & Menus

    QList<QAction*> inputPortsActions;
    QList<QAction*> outputPortsActions;
    QList <int> outputPortsIndex;

    //Global
    int globalState;

    contextType context;
    QString initPath;
    QString firmwarePath;
    QString previusMidiDevice;
    QString selectedMidiDevice;
    QString catchErrorString;
    QList <std::vector<unsigned char> > firmwareSysExMessages;

    uint8_t manufacturerHeader[6] = {SYSEX_ID0,SYSEX_ID1,SYSEX_ID2,0,0,0};

    QProgressDialog *progress;

    QString lastPath;
    QString lastFile;

    int flagFirmwareUpdate;
    int flagIgnoreMidiIn;
    int flagNumeratePorts;
    int flagReadyToUpload;
    int flagFillingPorts;
    int flagConnectingPorts;
    int flagWaitDialog;
    int flagSendingMIDI;

    void createActions();

    void createStylesheets();

    void updateStatus();

    int loadInitializationFile();

    void saveInitializationFile();

    bool fileExists(QString path);

    QString path();

    void setLastPath(QString path);

    void midiInit();

    void midiPortsList(QStringList *list, int direction);

    bool isBootDevice(QString device);

    bool isOutputPort(QString device);

    bool isConnected();

    void disconnectLoader();

    void createSysExFirmwareMessages(QString fileName);

    void firmwareUpdateBehavior(int parameter, unsigned char option=0);

    uint8_t encodeSysEx(QByteArray inData, std::vector<unsigned char> *outSysEx, uint8_t inLength);

    unsigned char getCheckSum(const std::vector<unsigned char> data, int len);

    unsigned char getCheckSum(uint8_t *data, int len);

    uint8_t getCRC8(const std::vector<unsigned char> data, int len);

    bool isSysExMessage(std::vector<unsigned char> message);

    int min(int,int);

    void sendSysEx(uint8_t size,uint8_t *data);

    void updateChannelsName();

protected:
    virtual void resizeEvent(QResizeEvent *event);
    virtual void closeEvent(QCloseEvent *event);
};



#endif // loader_H
