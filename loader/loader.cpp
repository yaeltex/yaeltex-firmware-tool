#include "loader.h"

loader::loader(QWidget *parent) : QMainWindow(parent),ui(new Ui::FirmwareLoader)
{
    this->setFixedSize(500,290);
    ui->setupUi(this);

    flagReadyToUpload = 0;
    flagFillingPorts = 0;
    flagConnectingPorts = 0;
    flagWaitDialog = 0;
    flagSendingMIDI = 0;
    flagFirmwareUpdate = 0;

    globalState=ytxState::INIT;

    selectedMidiDevice.clear();
    previusMidiDevice.clear();

    initPath = QStandardPaths::standardLocations(QStandardPaths::TempLocation).at(0) + "/init_data.ytx";

    if(loadInitializationFile())
    {
        lastPath = context.path;
        //this->setGeometry(context.x,context.y,context.width,context.height);
    }

    midiInit();

    createStylesheets();

    createActions();

    updateStatus();
}

void loader::resizeEvent(QResizeEvent* event)
{
   QMainWindow::resizeEvent(event);
}
void loader::closeEvent(QCloseEvent *event)
{
    if(globalState == ytxState::UPDATING)
    {
        event->ignore();

        QMessageBox msgBox(this);
        msgBox.setText(tr("Update in progress..."));
        msgBox.setInformativeText(tr("Exit?"));
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::Yes);
        msgBox.setButtonText(QMessageBox::Yes, tr("Yes"));
        msgBox.setButtonText(QMessageBox::No, tr("No"));

        if(msgBox.exec()==QMessageBox::Yes)
        {
            event->accept();
        }
    }
    else
    {
        if(isConnected()&&isBootDevice(selectedMidiDevice))
        {
            std::vector<unsigned char> message;

            message.clear();

            message.push_back(REQUEST_RST);

            for(int j=0;j<sizeof(manufacturerHeader);j++)
                message.insert(message.begin()+j,manufacturerHeader[j]);

            message.push_back(getCheckSum(message,message.size()));

            //SysEx Header
            message.insert(message.begin(),240);
            message.push_back( 247 );

            midiout->sendMessage( &message );
        }
        saveInitializationFile();
    }
}
int loader::min(int a,int b)
{
    if(a<b)
        return a;
    else
        return b;
}

loader::~loader()
{
    delete midiin;
    delete midiout;

    delete ui;
}




