#include "loader.h"

void loader::createActions()
{
    connect(ui->loadFilePushButton,SIGNAL(clicked()),this,SLOT(loadFirmwareFile()));
    connect(ui->sendPushButton,SIGNAL(clicked()),this,SLOT(firmwareUpdate()));
    connect(ui->actionEEPROM_erase,SIGNAL(triggered(bool)),this,SLOT(EEPROM_erase()));
    connect(ui->actionBuild_update_file,SIGNAL(triggered(bool)),this,SLOT(buildUpdate()));

    connect(ui->actionAbout,&QAction::triggered,[this]()
    {
        QMessageBox msgBox(this);
        msgBox.setText(tr("ytxFirmwareManager"));
        msgBox.setInformativeText((QString("Version: ")+QString(LOADER_VERSION)));
        msgBox.setStandardButtons(QMessageBox::Ok);

        msgBox.exec();
    });

    #ifndef __UNIX_JACK__
      ui->actionEEPROM_erase->setDisabled(1);
    #endif

    ui->actionExperimental_Features->setVisible(0);
}

void loader::buildUpdate()
{
    QString out = "error";

    QString targetFileName = QFileDialog::getSaveFileName(this, tr("Set output File"), this->path(), tr("BIN (*.bin)"));

    int fileSize;

    //Main file
    if(!targetFileName.isEmpty())
    {
        QString mainFileName = QFileDialog::getOpenFileName(this, tr("Open Main Firmware File"),this->path(),tr("BIN (*.bin)"));

        if(!mainFileName.isEmpty())
        {
            QFile main(mainFileName);

            if (main.open(QIODevice::ReadOnly))
            {
                fileSize = main.size();

                if(fileSize>YTX_MAIN_MIN_SIZE && fileSize<YTX_MAIN_MAX_SIZE)
                {
                    QString auxFileName = QFileDialog::getOpenFileName(this, tr("Open Aux Firmware File"),this->path(),tr("BIN (*.bin)"));

                    if(!auxFileName.isEmpty())
                    {
                        QFile aux(auxFileName);
                        if (aux.open(QIODevice::ReadOnly))
                        {
                            fileSize = aux.size();
                            if(fileSize>YTX_AUX_MIN_SIZE && fileSize<YTX_AUX_MAX_SIZE)
                            {
                                QFile target;
                                ytxUpdateHeaderType header;

                                memcpy(header.signature,FILE_SIGNATURE_ID,sizeof(FILE_SIGNATURE_ID));

                                if(!targetFileName.contains(".bin"))
                                    targetFileName.append(".bin");

                                target.setFileName(targetFileName);

                                if (target.open(QIODevice::WriteOnly))
                                {
                                    QByteArray blobMain,blobAux,blobHeader;
                                    QDataStream headerStream(&blobHeader, QIODevice::WriteOnly);

                                    blobMain = main.readAll();
                                    blobAux = aux.readAll();

                                    header.sizeMain = blobMain.size();
                                    header.sizeAux = blobAux.size();
                                    headerStream.writeRawData((char*)&header,sizeof(header));

                                    target.write(blobHeader);

                                    target.write(blobMain);

                                    target.write(blobAux);

                                    target.close();

                                    setLastPath(targetFileName);

                                    out = "\n\nBuild update success!";

                                }
                                else
                                    out = "\n\nCannot write target file";
                            }
                            else
                                out = "\n\nWrong Aux Firm file size!\n\nExpected 4kb to 28kb file";
                        }
                        else
                            out = "\n\nCannot open aux file";
                    }
                    else
                        out = "\n\nInvalid aux file name";
                }
                else
                    out = "\n\nWrong Main Firm file size!\n\nExpected 32kb to 224kb file";
            }
            else
                out = "\n\nCannot open main file";
        }
        else
            out = "\n\nInvalid main file name";

        QMessageBox msgBox(this);
        msgBox.setText(tr("Yaeltex V2 Firmware Uploader"));
        msgBox.setInformativeText(out);
        msgBox.setStandardButtons(QMessageBox::Ok);

        msgBox.exec();
    }
}

void loader::EEPROM_erase()
{
    QMessageBox msgBox(this);
    msgBox.setText(tr("Erase Confirmation"));
    msgBox.setInformativeText(tr("This action will erase all your controller's configuration. Are you brave enough?"));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::Yes);
    msgBox.setButtonText(QMessageBox::Yes, tr("Yes"));
    msgBox.setButtonText(QMessageBox::No, tr("No"));

    if(msgBox.exec()==QMessageBox::Yes)
    {
        if(midiout->isPortOpen())
        {
            std::vector<unsigned char> message;

            message.clear();

            message.push_back(REQUEST_EEPROM_ERASE);

            for(int j=0;j<sizeof(manufacturerHeader);j++)
                message.insert(message.begin()+j,manufacturerHeader[j]);

            message.push_back(getCheckSum(message,message.size()));

            //SysEx Header
            message.insert(message.begin(),240);
            message.push_back( 247 );

            midiout->sendMessage( &message );
        }
    }
}
