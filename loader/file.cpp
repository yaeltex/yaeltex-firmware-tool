#include "loader.h"

ytxHeaderType fileHeader = {FILE_SIGNATURE_ID,LOADER_VERSION};

int loader::loadInitializationFile()
{
    int fileOK =  (int)fileExists(initPath);

    if(fileOK)
    {
        QFile file;

        file.setFileName(initPath);


        if (file.open(QIODevice::ReadOnly) && file.size()==YTX_INI_FILE_SIZE)
        {
            QDataStream in(&file);
            ytxHeaderType header;

            in.readRawData((char*)&header,sizeof(fileHeader));

            if(memcmp(&header,&fileHeader,sizeof(fileHeader))==0)
            {
                in.readRawData((char*)&context,sizeof(context));
            }
        }
    }

    return fileOK;
}

void loader::saveInitializationFile()
{
    context.x = this->x();
    context.y = this->y();
    context.width = this->width();
    context.height = this->height();

    if(lastPath.length()>sizeof(context.path))
        lastPath.clear();
    strcpy(context.path,path().toStdString().c_str());

    QFile file;
    file.setFileName(initPath);


    if (file.open(QIODevice::WriteOnly))
    {
        QDataStream out(&file);
        //Header
        out.writeRawData((char*)&fileHeader,sizeof(fileHeader));
        //Data
        out.writeRawData((char*)&context,sizeof(context));

        file.close();
    }
}
bool loader::fileExists(QString path)
{
    QFileInfo check_file(path);
    // check if file exists and if yes: Is it really a file and no directory?
    if (check_file.exists() && check_file.isFile())
        return true;
    else
        return false;
}

QString loader::path()
{
    QString path;

    if(lastPath.isEmpty())
        path = QDir::homePath();
    else
        path = lastPath;
    return path;
}

void loader::setLastPath(QString path)
{
    lastPath = path;

    int index = lastPath.lastIndexOf("/");
    lastPath = lastPath.remove(index,lastPath.length()-index);
}

void loader::firmwareUpdate()
{
    if(flagReadyToUpload && globalState == ytxState::READY_TO_SEND)
        firmwareUpdateBehavior(BEGIN_FIRMWARE_UPTADE,REQUEST_UPLOAD_SELF);
}

void loader::loadFirmwareFile()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),this->path(),tr("BIN (*.bin)"));
    QString error;
    QFile file;
    file.setFileName(fileName);

    flagReadyToUpload = 0;

    catchErrorString.clear();

    if(globalState == ytxState::INIT)
        globalState = ytxState::DISCONNECTED;


    if(!fileName.isEmpty())
    {
        if(file.size()<=(YTX_MAIN_MAX_SIZE+YTX_AUX_MAX_SIZE+sizeof(ytxUpdateHeaderType)))
        {
            if(file.open(QIODevice::ReadOnly))
            {
                QDataStream in(&file);
                ytxUpdateHeaderType header;

                in.readRawData((char*)&header,sizeof(header));
                file.close();

                if(memcmp(header.signature,FILE_SIGNATURE_ID,sizeof(FILE_SIGNATURE_ID))==0)
                {
                    setLastPath(fileName);
                    firmwarePath = fileName;

                    if(isConnected())
                        globalState = ytxState::READY_TO_SEND;
                }
                else
                    catchErrorString = "Invalid file signature";
            }
            else
                catchErrorString = "Cannot open file";
        }
        else
            catchErrorString = "Invalid file size";
    }

    if(!catchErrorString.isEmpty())
    {
        firmwarePath.clear();

        if(isConnected())
            globalState = ytxState::CONNECTED;
    }

    if(!firmwarePath.isEmpty())
    {
        flagReadyToUpload = 1;
    }

//    if(!error.isEmpty())
//    {
//        QPixmap logo = QPixmap(":/img/logo.png");

//        QMessageBox msgBox(this);
//        msgBox.setIconPixmap(logo);
//        msgBox.setText(tr("Firmware update"));
//        msgBox.setInformativeText(error);
//        msgBox.setStandardButtons(QMessageBox::Ok);;

//        msgBox.exec();
//    }

    updateStatus();
}
