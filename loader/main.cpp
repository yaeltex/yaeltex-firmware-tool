#include "loader.h"
#include <QApplication>



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QPixmap pixmap(":/img/splash.png");
    QSplashScreen splash(pixmap);
    splash.show();

    // Load the embedded font.
    QString fontPath = ":/fonts/CamingoCode-Regular.ttf";
    int fontId = QFontDatabase::addApplicationFont(fontPath);
    if (fontId != -1)
    {
        QFont font("CamingoCode-Regular");
        a.setFont(font);
    }
    QFontDatabase::addApplicationFont(":/fonts/Rubik-Regular.ttf");
    QFontDatabase::addApplicationFont(":/fonts/Rubik-Medium.ttf");
    QFontDatabase::addApplicationFont(":/fonts/Rubik-Bold.ttf");
    QFontDatabase::addApplicationFont(":/fonts/Rubik-LightItalic.ttf");

    loader w;
    w.setWindowIcon(QIcon(":/img/icon.ico"));
    QTimer::singleShot(2500, &splash, SLOT(close()));
    QTimer::singleShot(2500, &w, SLOT(show()));

    return a.exec();
}
