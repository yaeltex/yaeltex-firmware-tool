#include "loader.h"

void loader::midiInit()
{
    // Create an api map.
    std::map<int, std::string> apiMap;
    apiMap[RtMidi::MACOSX_CORE] = "OS-X CoreMidi";
    apiMap[RtMidi::WINDOWS_MM] = "Windows MultiMedia";
    apiMap[RtMidi::UNIX_JACK] = "Jack Client";
    apiMap[RtMidi::LINUX_ALSA] = "Linux ALSA";
    apiMap[RtMidi::RTMIDI_DUMMY] = "RtMidi Dummy";

    midiin = 0;
    midiout = 0;

    //RtMidiIn constructor
    try
    {
        midiin = new RtMidiIn();
        std::cout << "\nCurrent input API: " << apiMap[ midiin->getCurrentApi() ] << std::endl;

    }
    catch ( RtMidiError &error )
    {
        error.printMessage();
        exit( EXIT_FAILURE );
    }

    // RtMidiOut constructor
    try {
        midiout = new RtMidiOut();
    }
    catch ( RtMidiError &error )
    {
        error.printMessage();
        exit( EXIT_FAILURE );
    }

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(midiPull()));
    timer->start(25);

    QTimer *timer2 = new QTimer(this);
    connect(timer2, SIGNAL(timeout()), this, SLOT(checkMidiConection()));
    timer2->start(1000);

    flagNumeratePorts = 1;

    inputPortsName = new QStringList();
    outputPortsName = new QStringList();
}

bool loader::isBootDevice(QString device)
{
    return device.contains("KilomuxBOOT");
}

bool loader::isOutputPort(QString device)
{
    return (!device.isEmpty()&&outputPortsName->contains(device));
}

bool loader::isConnected()
{
    return (midiin->isPortOpen() && midiout->isPortOpen());
}

void loader::connectToDevice(int index)
{
    if(flagConnectingPorts)
        return;

    flagConnectingPorts = 1;

    if(isConnected())
        disconnectLoader();

    globalState = ytxState::CONNECTING;
    updateStatus();

    if(index>=0 && index<midiout->getPortCount())
    {
        try
        {
            midiout->openPort(index);
        }
        catch ( RtMidiError &error )
        {
            disconnectLoader();

            QMessageBox msgBox(this);
            msgBox.setText(tr("MIDI Device"));
            msgBox.setInformativeText(tr("Device busy or taken by another program!"));
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.exec();

            flagConnectingPorts = 0;

            return;
        }

        if(midiout->isPortOpen())
        {
            selectedMidiDevice = outputPortsName->at(index);

            int inputIndex=-1;

            QList<int> outputPortindex;
            for(int i=0;i<outputPortsName->count();i++)
            {
                if(outputPortsName->at(i)==selectedMidiDevice)
                {
                    outputPortindex<<i;
                }
            }

            int outFilteredIndex = outputPortindex.indexOf(index);

            QList<int> inputPortindex;
            for(int i=0;i<inputPortsName->count();i++)
            {
                if(inputPortsName->at(i).contains(selectedMidiDevice))
                {
                    inputPortindex<<i;
                }
            }

            if(outFilteredIndex<inputPortindex.count())
                inputIndex = inputPortindex[outFilteredIndex];

            if(inputIndex!=-1)
            {
                flagNumeratePorts = 0;

                try
                {
                    midiin->openPort(inputIndex);
                }
                catch ( RtMidiError &error )
                {
                    disconnectLoader();

                    QMessageBox msgBox(this);
                    msgBox.setText(tr("MIDI Device"));
                    msgBox.setInformativeText(tr("Device busy or taken by another program!"));
                    msgBox.setDefaultButton(QMessageBox::Ok);
                    msgBox.exec();

                    return;
                }

                // Don't ignore sysex, timing, or active sensing messages.
                midiin->ignoreTypes( false, false, false );

                if(isBootDevice(selectedMidiDevice))
                {
                    if(flagReadyToUpload)
                        globalState = ytxState::READY_TO_SEND;
                    else
                        globalState = ytxState::CONNECTED;
                }
                else
                {

                }

                flagNumeratePorts = 1;

            }
            else
            {
                qDebug()<<"Imposible conectar puerto de entrada";
            }

            updateStatus();
        }
        else
        {
            qDebug()<<"Imposible conectar puerto de salida";
        }

    }

    flagConnectingPorts = 0;
}

void loader::disconnectLoader()
{
    midiin->closePort();
    midiout->closePort();

    selectedMidiDevice.clear();

    flagNumeratePorts = 1;

    globalState = ytxState::DISCONNECTED;
    updateStatus();
}

void loader::resetDeviceIntoBootloader()
{
    if(midiout->isPortOpen())
    {
        std::vector<unsigned char> message;

        flagSendingMIDI = 1;

        message.clear();

        message.push_back(REQUEST_BOOT_MODE);

        for(int j=0;j<sizeof(manufacturerHeader);j++)
            message.insert(message.begin()+j,manufacturerHeader[j]);

        //SysEx Header
        message.insert(message.begin(),240);
        message.push_back( 247 );

        try
        {
             midiout->sendMessage( &message );
        }
        catch ( RtMidiError &error )
        {
            error.printMessage();
        }

        flagSendingMIDI = 0;
    }
}

void loader::midiSendTimeout()
{
    if(flagSendingMIDI)
    {
        disconnectLoader();
        flagSendingMIDI = 0;
    }
}
void loader::checkMidiConection()
{
    static int tries=0;

    if(flagNumeratePorts)
    {
        tries=0;
        searchPorts();

        if(isConnected())
        {
            if(isOutputPort(selectedMidiDevice))
            {
                if(!isBootDevice(selectedMidiDevice))
                {
                  //QTimer::singleShot(200, this, SLOT(midiSendTimeout()));
                  QTimer::singleShot(500, this, SLOT(resetDeviceIntoBootloader()));
                }
            }
            else
            {
                if(!selectedMidiDevice.isEmpty()&&!isBootDevice(selectedMidiDevice))
                    previusMidiDevice = selectedMidiDevice;

                disconnectLoader();
            }

        }
        else
        {
            //Reconnection to previus device
            if(isOutputPort(previusMidiDevice))
            {
                connectToDevice(outputPortsName->indexOf(previusMidiDevice));
            }
            //Connection to boot device
            else
            {
                for (int i=0;i<outputPortsName->size();i++)
                {
                    if(isBootDevice(outputPortsName->at(i)))
                    {
                        connectToDevice(i);
                        break;
                    }
                }
            }
        }
    }

//    if(flagConnectingPorts)
//    {
//        qDebug()<<"connnecting..."<<QTime().currentTime();
//        if(!flagWaitDialog)
//        {
//            qDebug()<<"Tries: "<<tries;
//            if(++tries>=3)
//            {
//                qDebug()<<"Disconect";
//                disconnectLoader();
//                searchPorts();
//            }
//        }
//    }
}

void loader::on_midiportsCombo_currentIndexChanged(int index)
{
    if(flagFillingPorts)
        return;

    if(index)
        connectToDevice(outputPortsIndex[index-1]);
    else
    {
        previusMidiDevice.clear();
        disconnectLoader();
    }
    flagFillingPorts = 1;
    //return to select option
    ui->midiportsCombo->setCurrentIndex(0);
    flagFillingPorts = 0;
}

void loader::searchPorts()
{
    midiPortsList(outputPortsName,0);
    midiPortsList(inputPortsName,1);

    if(outputPortsName->count())
    {
        flagFillingPorts = 1;

        ui->midiportsCombo->clear();
        ui->midiportsCombo->addItem(tr("Select"));
        for(int i=0;i<outputPortsName->count();i++)
        {
            if(!outputPortsName->at(i).contains("Microsoft GS Wavetable"))
            {
                outputPortsIndex<<i;
                ui->midiportsCombo->addItem(outputPortsName->at(i));
            }
        }

        flagFillingPorts = 0;
    }
}

void loader::midiPortsList(QStringList *list,int direction)
{
    std::string portName;

    unsigned int nPorts;

    list->clear();

    if(direction)
        nPorts = midiin->getPortCount();
    else
        nPorts = midiout->getPortCount();

    if(nPorts)
    {
        for ( unsigned int i=0; i<nPorts; i++ )
        {
            try
            {
                if(direction)
                    portName = midiin->getPortName(i);
                else
                    portName = midiout->getPortName(i);
            }
            catch ( RtMidiError &error )
            {
              error.printMessage();
            }
            QString port = QString().fromStdString(portName);

            list->append(port.left(port.lastIndexOf(QChar(' '))));
        }
    }
}

void loader::updateStatus()
{
    QString device,file;

    if(selectedMidiDevice.isEmpty())
    {
        device = tr("Device: none");
    }
    else
    {
        device = tr("Device: ") + selectedMidiDevice;
    }

    if(flagReadyToUpload)
    {
        QString firmwareName = firmwarePath.replace(QChar('\\'),QChar('/'));
        file = "File: "+firmwareName.remove(0,firmwareName.lastIndexOf(QChar('/'))+1);

        ui->labelCheck2->setPixmap(QPixmap(":/img/blue-check.png"));
    }
    else
    {
        if(catchErrorString.isEmpty())
            file = "File: none";
        else
            file = QString("File: none (%1)").arg(catchErrorString);

        ui->labelCheck2->clear();
    }

    ui->deviceInfo->setText(device);
    ui->deviceInfo->setStyleSheet(rubykRegularWhiteLabelStylesheet);

    ui->fileInfo->setText(file);
    ui->fileInfo->setStyleSheet(rubykRegularWhiteLabelStylesheet);

    if(globalState==ytxState::CONNECTED || globalState==ytxState::READY_TO_SEND || globalState==ytxState::UPDATING)
    {
        ui->labelCheck1->setPixmap(QPixmap(":/img/blue-check.png"));
    }
    else
    {
        ui->labelCheck1->clear();
    }

    switch (globalState)
    {
        case ytxState::INIT:
            ui->statusLabel->setText("Welcome!");
            break;
        case ytxState::DISCONNECTED:
            ui->statusLabel->setText("Disconnected");
            break;
        case ytxState::CONNECTING:
            ui->statusLabel->setText("Connecting");
            break;
        case ytxState::CONNECTED:
            ui->statusLabel->setText("Connected");
            break;
        case ytxState::READY_TO_SEND:
            ui->statusLabel->setText("Ready!");
            break;
        case ytxState::UPDATING:
            ui->statusLabel->setText("Updating...");
            break;
        case ytxState::UPLOADED:
            ui->statusLabel->setText("Done!");
            break;
        default:
            break;
    }

    ui->actionEEPROM_erase->setEnabled(isConnected());

    bool ready = isConnected()&isBootDevice(selectedMidiDevice)&flagReadyToUpload;

    if(ready)
    {
        ui->statusLabel->setStyleSheet(rubykRegularBlueLabelStylesheet);
    }
    else
    {
        ui->statusLabel->setStyleSheet(rubykRegularWhiteLabelStylesheet);
    }

    ui->sendPushButton->setEnabled(ready);

}

uint8_t loader::getCRC8(const std::vector<unsigned char> data, int len)
{
  int i=0;
  uint8_t crc = 0x00;
  while (len--)
  {
    uint8_t extract = data[i];i++;
    for (uint8_t tempI = 8; tempI; tempI--)
    {
      uint8_t sum = (crc ^ extract) & 0x01;
      crc >>= 1;
      if (sum)
        crc ^= 0x8C;

      extract >>= 1;
    }
  }
  return crc&0x7F;
}

unsigned char loader::getCheckSum(const std::vector<unsigned char> data, int len)
{
    unsigned char checksum = 0;

    for (int i = 0; i < len; i++)
        checksum ^= data[i];

    return checksum &= 0x7f;
}

unsigned char loader::getCheckSum(uint8_t *data, int len)
{
    unsigned char checksum = 0;

    for (int i = 0; i < len; i++)
        checksum ^= data[i];

    return checksum &= 0x7f;
}
bool loader::isSysExMessage(std::vector<unsigned char> message)
{
    if(message[0]==240 && message[message.size()-1]==247)
        return true;

    return false;
}

void loader::midiPull()
{
    if(midiin->isPortOpen())
    {
        std::vector<unsigned char> message;
        double stamp = midiin->getMessage( &message );

        int nBytes = message.size();

        if(nBytes>0 && isSysExMessage(message))
        {

            //erase sysex delimiters
            message.erase(message.begin());
            message.erase(message.end()-1);

            //bootloader order
            if(message[0] == SYSEX_ID0 && message[1]==SYSEX_ID1 &&  message[2]==SYSEX_ID2)
            {
                if(flagFirmwareUpdate)
                {
                    if(message[ytxIOStructure::MESSAGE_STATUS]==STATUS_ACK)
                        firmwareUpdateBehavior(ACK_FIRMWARE_UPTADE);
                    else if(message[ytxIOStructure::MESSAGE_STATUS]==STATUS_NAK)
                        firmwareUpdateBehavior(NAK_FIRMWARE_UPTADE);
                }
            }
        }
        else
        {

            for (int i=0; i<nBytes; i++ )
              qDebug() << "Byte " << i << " = " << (int)message[i] << ", ";
            if ( nBytes > 0 )
              qDebug() << "stamp = " << stamp;


        }
    }
}

void loader::sendSysEx(uint8_t size,uint8_t *data)
{
    std::vector<unsigned char> sysexBlock;
    sysexBlock.clear();
    for (int i=0;i<size;i++)
        sysexBlock.push_back(data[i]);

    sysexBlock.insert(sysexBlock.begin(),240);
    sysexBlock.push_back( 247 );
    midiout->sendMessage( &sysexBlock );
}


