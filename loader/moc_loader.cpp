/****************************************************************************
** Meta object code from reading C++ file 'loader.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "loader.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'loader.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_loader_t {
    QByteArrayData data[14];
    char stringdata0[207];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_loader_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_loader_t qt_meta_stringdata_loader = {
    {
QT_MOC_LITERAL(0, 0, 6), // "loader"
QT_MOC_LITERAL(1, 7, 8), // "midiPull"
QT_MOC_LITERAL(2, 16, 0), // ""
QT_MOC_LITERAL(3, 17, 12), // "EEPROM_erase"
QT_MOC_LITERAL(4, 30, 11), // "buildUpdate"
QT_MOC_LITERAL(5, 42, 14), // "firmwareUpdate"
QT_MOC_LITERAL(6, 57, 16), // "loadFirmwareFile"
QT_MOC_LITERAL(7, 74, 15), // "connectToDevice"
QT_MOC_LITERAL(8, 90, 5), // "index"
QT_MOC_LITERAL(9, 96, 25), // "resetDeviceIntoBootloader"
QT_MOC_LITERAL(10, 122, 15), // "midiSendTimeout"
QT_MOC_LITERAL(11, 138, 11), // "searchPorts"
QT_MOC_LITERAL(12, 150, 18), // "checkMidiConection"
QT_MOC_LITERAL(13, 169, 37) // "on_midiportsCombo_currentInde..."

    },
    "loader\0midiPull\0\0EEPROM_erase\0buildUpdate\0"
    "firmwareUpdate\0loadFirmwareFile\0"
    "connectToDevice\0index\0resetDeviceIntoBootloader\0"
    "midiSendTimeout\0searchPorts\0"
    "checkMidiConection\0"
    "on_midiportsCombo_currentIndexChanged"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_loader[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   69,    2, 0x08 /* Private */,
       3,    0,   70,    2, 0x08 /* Private */,
       4,    0,   71,    2, 0x08 /* Private */,
       5,    0,   72,    2, 0x08 /* Private */,
       6,    0,   73,    2, 0x08 /* Private */,
       7,    1,   74,    2, 0x08 /* Private */,
       9,    0,   77,    2, 0x08 /* Private */,
      10,    0,   78,    2, 0x08 /* Private */,
      11,    0,   79,    2, 0x08 /* Private */,
      12,    0,   80,    2, 0x08 /* Private */,
      13,    1,   81,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    8,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    8,

       0        // eod
};

void loader::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<loader *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->midiPull(); break;
        case 1: _t->EEPROM_erase(); break;
        case 2: _t->buildUpdate(); break;
        case 3: _t->firmwareUpdate(); break;
        case 4: _t->loadFirmwareFile(); break;
        case 5: _t->connectToDevice((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->resetDeviceIntoBootloader(); break;
        case 7: _t->midiSendTimeout(); break;
        case 8: _t->searchPorts(); break;
        case 9: _t->checkMidiConection(); break;
        case 10: _t->on_midiportsCombo_currentIndexChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject loader::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_loader.data,
    qt_meta_data_loader,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *loader::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *loader::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_loader.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int loader::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 11)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 11;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
